# 👍TempMailo - disposable mail  

The **temporary email address** service enables you to register in websites that require an email address. This allows you to avoid any unwanted and spam mails.

[tempmailo.com](https://tempmailo.com/)
